locals {
  common_tags = {
    Owner       = "danielfpvieira@gmail.com"
    Project     = "DevOps"
    Environment = var.env
  }
}

