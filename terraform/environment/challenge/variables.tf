variable "region" {
  default = "us-east-1"
}

variable "env" {
}

variable "cidr_vpc" {
}

variable "public1" {
}

variable "public2" {
}

variable "private1" {
}

variable "private2" {
}

variable "instance_type"{
}