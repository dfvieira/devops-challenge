resource "kubernetes_cluster_role" "traefik-ingress-controller" {
  metadata {
    name = "traefik-ingress-controller"
  }

  rule {
    api_groups = [""]
    resources  = ["services", "endpoints", "secrets"]
    verbs      = ["get", "list", "watch"]
  }
  rule {
    api_groups = ["extensions"]
    resources  = ["ingresses"]
    verbs      = ["get", "list", "watch"]
  }
  rule {
    api_groups = ["extensions"]
    resources  = ["ingresses/status"]
    verbs      = ["update"]
  }
}

resource "kubernetes_cluster_role_binding" "traefik-ingress-controller" {
  metadata {
    name = "traefik-ingress-controller"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "traefik-ingress-controller"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "traefik-ingress-controller"
    namespace = "kube-system"
  }
}

resource "kubernetes_service_account" "traefik-ingress-controller" {
  metadata {
    name = "traefik-ingress-controller"
    namespace = "kube-system"
  }
}

resource "kubernetes_deployment" "traefik-ingress" {
  metadata {
    name = "traefik-ingress"
    namespace = "kube-system"
    labels = {
      app = "traefik"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "traefik"
      }
    }

    template {
      metadata {
        labels = {
          app = "traefik"
        }
      }

      spec {
        service_account_name = "traefik-ingress-controller"
        container {
          image = "traefik:v2.2"
          name  = "traefik"
        args = [
            "--log.level=INFO",
            "--api",
            "--api.insecure",
            "--entrypoints.web.address=:80",
            "--providers.kubernetesingress",
            "--accesslog"
        ]
        port {
        container_port = 80
        name = "web"
      }
        port {
        container_port = 8080
        name = "admin"
      }
        }
      }
    }
  }
}

resource "kubernetes_service" "traefik-ingress" {
  metadata {
    name = "traefik-ingress"
    namespace = "kube-system"
  }
  spec {
    selector = {
      app = "traefik"
    }
    port {
      port        = 80
      target_port = 80
      name = "web"
    }
    port {
      port        = 8080
      target_port = 8080
      name = "admin"
    }

    type = "LoadBalancer"
  }
}