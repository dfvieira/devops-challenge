output "ecr-web" {
    value = module.ecr-web.ecr_url
}

output "ecr-api" {
    value = module.ecr-api.ecr_url
}