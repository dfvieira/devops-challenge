provider "aws" {
  region  = var.region
  version = "2.69.0"
}

module "ecr-web" {
  source = "../../modules/ecr"
  ecr_name = "frontend-web"
  tags = merge(
    local.common_tags)
}

module "ecr-api" {
  source = "../../modules/ecr"
  ecr_name = "backend-api"
  tags = merge(
    local.common_tags)
}