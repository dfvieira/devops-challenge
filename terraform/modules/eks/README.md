# EKS

Documentação para uso do módulo
## Apontamento

Realizar o clone do repositório ou apontar o source para o repo do git



## Utilização 

Criar o diretório com o nome de projeto ou ambiente, criar os arquivos, provider.tf, vars.tf, backend.tf, output.tf e por fim abaixo o exemplo de arquivo para utilização do módulo cujo o nome pode ser ambiente "BBCE-SANDBOX.tf" :

```
module "eks" {
  source                               = "../module/k8s"
  cluster_name                         = "${var.cluster-name}-${var.ENV}"
  subnets                              = ["${module.vpc.subnet-app-0}", "${module.vpc.subnet-app-1}", "${module.vpc.subnet-app-2}", "${module.vpc.subnet-app-3}"] #MUDAR PARA O ID
  vpc_id                               = "${module.vpc.vpc}" #MUDAR PARA O ID
  map_roles                            = "${var.map_roles}"
  manage_aws_auth                      = true
  worker_additional_security_group_ids = ["${module.SG.sgoutput}"]
  cluster_enabled_log_types            = ["api", "audit", "authenticator", "controllerManager", "scheduler"]
  key_name                             = "${var.keyName}"
  cluster_version                      = "${var.versionCluster}"
  cluster_endpoint_public_access_cidrs = ["0.0.0.0/0"]
  worker_groups = [
    {
      instance_type                 = "%{if var.ENV == "prod" || var.ENV == "stress-test"}${var.AWS_TYPE_INSTANCE}%{else}t2.medium%{endif}"
      asg_max_size                  = "%{if var.ENV == "prod" || var.ENV == "stress-test"}${var.DMAXEKS}%{else}3%{endif}"
      asg_desired_capacity          = "%{if var.ENV == "prod" || var.ENV == "stress-test"}${var.DMIN}%{else}2%{endif}"
      kubelet_extra_args            = "--node-labels=ehub=microservices"
      additional_security_group_ids = ["${module.SG.sgoutput}"]
    }
  ]
  tags = {
    Name                                                   = "${var.cluster-name}-${var.ENV}"
    Terraform                                              = true
    APP                                                    = "EHUB"
    Projeto                                                = "EHUB"
    Requerente                                             = "${var.requerente}"
    Ambiente                                               = "${var.ENV}"
    "kubernetes.io/cluster/${var.cluster-name}-${var.ENV}" = "shared"

  }
}


terraform init 
terraform validate
terraform plan -out exemplo
terraform apply exemplo 
