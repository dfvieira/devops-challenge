# Devops Challenge

Para confecção desde documento estou partindo do pressuposto que o executor tem conhecimentos das ferramentas utilizadas abaixo.

**Para provisionamento do desafio utilizei os provedores, ferramentas e recursos:**
Terraform (para provisionamento de recursos)  
AWS  
BitBucket  
  
**Recursos do ambiente AWS:** 
Route 53  
VPC  
Subnets (pública e privada)  
Security Groups  
Internet Gateway  
Nat Gateway  
Route Table  
Elastic IP  
EKS  
EC2  
ECR  
ELB  
 
**Recursos do EKS:**  
Conexão com o ECR 
Configuração do Ingress Traefik para as aplicações 
Deploy de duas aplicações (frontend-web e backend-api-rest)  

**Aplicações do Challenge:**  
Aplicação de modelo simples para backend-api-rest:  
https://imasters.com.br/back-end/api-http-rest-conceito-e-exemplo-em-node-js
 
Para o frontend estático criei um apache simples para exemplificar. 
 
**Execução do terraform para criação dos recursos**  
Executar os scripts terraform a partir da branch master.   
Para criação dos recursos utilizei o terraform versão 0.12.24 e o aws cli v1. 
O único modulo terraform que não criei foi o do eks, onde usei o modulo oficial.  
https://github.com/terraform-aws-modules/terraform-aws-eks  

Para instalação siga os passos do link abaixo: 
https://learn.hashicorp.com/terraform/getting-started/install  
 
Para instalação do aws cli siga os passos do link abaixo: 
https://docs.aws.amazon.com/pt_br/cli/latest/userguide/install-cliv1.html  
 
Antes de realizar as criações é necessario ter uma conta aws com acesso programático e permissão de administrador para conseguir criar os recursos, ao acessar a aws criar uma key_pair para configuração no cluster EKS.  

Antes de realizar a criação do ambiente deve-se ter um dominio valido para ser usado para DEV e um dominio valido para ser usado em PROD.  
 
Os scripts terraform estão criados com estruturação modularizada e separação de provisionamento de ambientes por variaveis, a criação dos recursos será realizada por um gatilho manual conforme as instruções abaixo: 
 
**Criação do ambiente de DEV:**  
Acessar a estrutura de pastas para criação do ambiente.  
terraform/environment/challenge  
Executar os comandos abaixo para aplicação dos scripts:  
terraform init  
terraform apply -var-file=dev.tfvars -state=dev.tfstate  
 
**Criação do ambiente de PRD:**  
Acessar a estrutura de pastas para criação do ambiente.  
terraform/environment/challenge  
Executar os comandos abaixo para aplicação dos scripts:  
terraform init  
terraform apply -var-file=prod.tfvars -state=prod.tfstate  
 
Após a criação dos recursos, acesse o cluster EKS para validação do ambiente.  
**Executar os comandos abaixo para validar o ambiente de DEV:**  
aws eks --region us-east-1 update-kubeconfig --name eks-dev  
kubectl get pods --all-namespaces  
**Obs: $path é o caminho onde baixou o repositorio git do projeto.**  
Para pegar a informação do nome DNS do ELB execute o comando abaixo:  
kubectl get services -n kube-system  
  
**Executar os comandos abaixo para validar o ambiente de PROD:**  
aws eks --region us-east-1 update-kubeconfig --name eks-prod  
kubectl get pods --all-namespaces  
**Obs: $path é o caminho onde baixou o repositorio git do projeto.**  
Para pegar a informação do nome DNS do ELB execute o comando abaixo:  
kubectl get services -n kube-system  
  
**Criação do ECR:**  
Para criar os container registry's que irão receber as imagens acessar a estrutura de pastas.  
terraform/environment/ecr  
Executar os comandos abaixo para aplicação dos scripts:  
terraform init  
terraform apply  
Será exposto no output de informações as URL's dos repositórios ECR  
  
**Criação de entrada DNS no Route53 para o LB das aplicações:**  
Para criar o DNS é necessário já ter executado os passos anteriores e a criação do ingress traefik no EKS.  
terraform/environment/route53  
Executar os comandos abaixo para aplicação dos scripts:  
terraform init  
Criação da entrada DNS para DEV  
Ajustar os valores das variaveis para os valores dos recursos que foram criados nos passos anteriores no arquivo dev.tfvars:  
zone_id = ID da Zona DNS criada, essa informação é exportada no output do terraform ao final da criação dos recursos.  
source_domain = É o nome DNS do NLB que foi criado ao aplicar o deploy do traefik no EKS. Para pegar essa informação digite o comando:  
kubectl get services -n kube-system  
target_domain = Nesta variável é informado o nome dns para a aplicação proposta  
terraform apply -var-file=dev.tfvars -state=dev.tfstate   
  
Criação da entrada DNS para PROD  
Ajustar os valores das variaveis para os valores dos recursos que foram criados nos passos anteriores no arquivo prod.tfvars:  
zone_id = ID da Zona DNS criada, essa informação é exportada no output do terraform ao final da criação dos recursos.  
source_domain = É o nome DNS do NLB que foi criado ao aplicar o deploy do traefik no EKS. Para pegar essa informação digite o comando:  
kubectl get services -n kube-system  
target_domain = Nesta variável é informado o nome dns para a aplicação proposta  
terraform apply -var-file=prod.tfvars -state=prod.tfstate  
  
  
**CI/CD**
  
Para execução dos pipelines de CI/CD deve-se utilizar a branch de release para realizarmos os passos de build das imagens docker, e deploy das aplicações no EKS.  
Adicionar as variaveis abaixo no Repository Variables default do projeto no Bitbucket:  
AWS_ACCESS_KEY_ID = Access Key com acesso de Administrador do EKS.  
AWS_SECRET_ACCESS_KEY = Secret Key com acesso de Administrador do EKS.  
AWS_DEFAULT_REGION = Região da AWS onde os recursos estão instalados.  
DEV_AWS_EKS_NAME = Nome do EKS de DEV  
PROD_AWS_EKS_NAME = Nome do EKS de PROD  
URL_WEB = URL do ECR do frontend web estático  
URL_API = URL do ECR do backend api rest   
Em Deployments adicionar as variaveis especificas de cada ambiente.  
development  
API_URL = Url que irá responder as aplicações, ex: apps.dev.danielvieira.io  
producrion  
API_URL = Url que irá responder as aplicações, ex: apps.prod.danielvieira.io  
Após realizar essas configurações é so realizar o commit e push das alterações na branch release e o deploy automatico será realizado nos ambientes.  
Para testar as aplicações utilizei os links:  
http://apps.dev.danielvieira.io/web/index.html  
http://apps.dev.danielvieira.io/api  
  
http://apps.prod.danielvieira.io/web/index.html  
http://apps.prod.danielvieira.io/api  